package org.vavra.leetcode.problem005;

class Solution {
    public String longestPalindrome(String s) {
        if (s.isEmpty()) {
            return "";
        }
        if (s.length() == 1) {
            return s;
        }

        int longestOddCentre = 0;
        int longestOddSize = 0;

        int longestEvenCentre = 0;
        int longestEvenSize = -1;

        for (int i = 0; i < s.length(); ++i) {
            // Odd-size palindrome.
            int oddSize = 0;
            while (i-oddSize >= 0 && i+oddSize < s.length() && s.charAt(i-oddSize) == s.charAt(i+oddSize)) {
                ++oddSize;
            }
            --oddSize;
            if ((1+2*oddSize) > 1+2*longestOddSize) {
                longestOddCentre = i;
                longestOddSize = oddSize;
            }
            // Even-size palindrome.
            int evenSize = 0;
            while (i-evenSize >= 0 && i+1+evenSize < s.length() && s.charAt(i-evenSize) == s.charAt(i+1+evenSize)) {
                ++evenSize;
            }
            --evenSize;
            if (((evenSize+1)*2) > (longestEvenSize+1)*2) {
                longestEvenCentre = i;
                longestEvenSize = evenSize;
            }
        }

        if (longestEvenSize >= longestOddSize) {
            // return even size palindrome
            return s.substring(longestEvenCentre-longestEvenSize, longestEvenCentre+1+longestEvenSize+1);
        } else {
            // return odd size palindrome
            return s.substring(longestOddCentre-longestOddSize, longestOddCentre+longestOddSize+1);
        }
    }
}
