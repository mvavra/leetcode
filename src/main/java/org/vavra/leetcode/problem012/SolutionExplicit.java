package org.vavra.leetcode.problem012;

class SolutionExplicit {
    public String intToRoman(int num) {
        StringBuilder sb = new StringBuilder(15);

        /*
        3___ ... MMM
        2___ ... MM
        1___ ... M
        0___ ... ""

        _9__ ... CM
        _8__ ... DCCC
        _7__ ... DCC
        _6__ ... DC
        _5__ ... D
        _4__ ... CD
        _3__ ... CCC
        _2__ ... CC
        _1__ ... C
        _0__ ... ""

        __9_ ... XC
        __8_ ... LXXX
        __7_ ... LXX
        __6_ ... LX
        __5_ ... L
        __4_ ... XL
        __3_ ... XXX
        __2_ ... XX
        __1_ ... X
        __0_ ... ""

        ___9 ... IX
        ___8 ... VIII
        ___7 ... VII
        ___6 ... VI
        ___5 ... V
        ___4 ... IV
        ___3 ... III
        ___2 ... II
        ___1 ... I
        ___0 ... ""

         */

        String[] a4 = new String[] { "", "M", "MM", "MMM" };
        String[] a3 = new String[] { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
        String[] a2 = new String[] { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
        String[] a1 = new String[] { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };

        sb.append(a4[num / 1000]);
        num %= 1000;

        sb.append(a3[num / 100]);
        num %= 100 ;

        sb.append(a2[num / 10]);
        num %= 10;

        sb.append(a1[num]);

        return sb.toString();
    }
}