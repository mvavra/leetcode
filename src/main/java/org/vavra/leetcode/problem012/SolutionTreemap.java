package org.vavra.leetcode.problem012;

import java.util.Map;
import java.util.TreeMap;

class SolutionTreemap {
    private static final TreeMap<Integer, String> MAPPING = new TreeMap<>();

    static {
        MAPPING.put(1000, "M");
        MAPPING.put(900, "CM");
        MAPPING.put(500, "D");
        MAPPING.put(400, "CD");
        MAPPING.put(100, "C");
        MAPPING.put(90, "XC");
        MAPPING.put(50, "L");
        MAPPING.put(40, "XL");
        MAPPING.put(10, "X");
        MAPPING.put(9, "IX");
        MAPPING.put(5, "V");
        MAPPING.put(4, "IV");
        MAPPING.put(1, "I");
    }

    public String intToRoman(int num) {
        StringBuilder sb = new StringBuilder(15);

        while (num != 0) {
            Map.Entry<Integer, String> floorEntry = MAPPING.floorEntry(num);
            sb.append(floorEntry.getValue());
            num -= floorEntry.getKey();
        }

        return sb.toString();
    }
}