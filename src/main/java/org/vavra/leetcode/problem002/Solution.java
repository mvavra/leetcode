package org.vavra.leetcode.problem002;

public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode first = null;
        ListNode last = null;
        int carry = 0;
        while (l1 != null || l2 != null || carry != 0) {
            int sum = 0;
            if (l1 != null) {
                sum += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                sum += l2.val;
                l2 = l2.next;
            }
            sum += carry;

            ListNode newNode = new ListNode(sum % 10);
            if (last != null) {
                last.next = newNode;
            }
            last = newNode;
            if (first == null) {
                first = newNode;
            }

            carry = sum / 10;
        }
        return first;
    }
}
