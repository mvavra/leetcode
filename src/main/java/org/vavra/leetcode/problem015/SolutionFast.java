package org.vavra.leetcode.problem015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SolutionFast {

    public List<List<Integer>> threeSum(int[] nums) {

        /*
         * Sort input array.
         *
         * O(n log n)
         *
         */
        Arrays.sort(nums);

        /*
         * Generate results.
         *
         * O(n^2)
         *
         */
        List<List<Integer>> results = new LinkedList<>();

        int i = 0;
        while (i < nums.length - 2) {
            final int numA = nums[i];
            final int target = -1 * numA;

            int j = i + 1;
            int k = nums.length - 1;

            while (j < k) {
                final int numB = nums[j];
                final int numC = nums[k];
                final int sum = numB + numC;

                if (sum == target) {
                    List<Integer> result = new ArrayList<>(3);
                    result.add(numA);
                    result.add(numB);
                    result.add(numC);
                    results.add(result);

                    // increment j, skip duplicates
                    while ((j < k) && (nums[j] == numB)) {
                        ++j;
                    }

                    // decrement k, skip duplicates
                    while ((j < k) && (nums[k] == numC)) {
                        --k;
                    }
                }
                else if (sum > target) {
                    --k;
                }
                else if (sum < target) {
                    ++j;
                }
            }

            // increment i, skip duplicates
            while ((i < nums.length - 2) && nums[i] == numA) {
                ++i;
            }
        }

        return results;
    }
}
