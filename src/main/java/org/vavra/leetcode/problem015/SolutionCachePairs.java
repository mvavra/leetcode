package org.vavra.leetcode.problem015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

class SolutionCachePairs {

    public Consumer<String> LOG = msg -> {};

    public List<List<Integer>> threeSum(int[] nums) {

        /*
         * Sort the input array.
         *
         * O(n * log(n))
         *
         */

        LOG.accept("Sorting input array.");

        int[] numsSorted = nums.clone();
        Arrays.sort(numsSorted);

        /*
         * Generate pairs cache.
         *
         * O(n^2)
         *
         */

        LOG.accept("Generating pairs cache.");

        Map<Integer, List<Pair>> pairs = new HashMap<>();
        for (int i = 0; i < numsSorted.length-1; ++i) {
            for (int j = i+1; j < numsSorted.length; ++j) {
                final int twoSum = numsSorted[i] + numsSorted[j];
                List<Pair> list = pairs.get(twoSum);
                if (list == null) {
                    list = new LinkedList<>();
                    pairs.put(twoSum, list);
                }
                list.add(new Pair(i, j, numsSorted[i], numsSorted[j]));
            }
        }

        /*
         * Generate triplets that sum up to zero.
         *
         * O(n)
         *
         */

        LOG.accept("Generating result triplets.");

        Set<ArrayList<Integer>> triplets = new LinkedHashSet<>();
        for (int i = 0; i < numsSorted.length; ++i) {
            final int numC = numsSorted[i];
            final int twoSum = 0 - numC;
            List<Pair> list = pairs.get(twoSum);
            if (list != null) {
                for (Pair pair : list) {
                    if (i < pair.pos1) {
                        ArrayList<Integer> triplet = new ArrayList<>(3);
                        triplet.add(numC);
                        triplet.add(pair.val1);
                        triplet.add(pair.val2);
                        triplets.add(triplet);
                    }
                }
            }
        }

        LOG.accept("Done.");

        /*
         * Convert to result.
         */
        return new LinkedList<>(triplets);
    }

    private static class Pair {
        public int pos1;
        public int pos2;
        public int val1;
        public int val2;
        public Pair(int pos1, int pos2, int val1, int val2) {
            this.pos1 = pos1;
            this.pos2 = pos2;
            this.val1 = val1;
            this.val2 = val2;
        }

    }

}