package org.vavra.leetcode.problem015;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

class Solution {
    public Consumer<String> LOG = msg -> {};

    public List<List<Integer>> threeSum(int[] nums) {

        List<List<Integer>> result = new LinkedList<>();

        /*
         * Sort the input array.
         *
         * O(n * log(n))
         *
         */
        LOG.accept("Sorting input array.");

        int[] numsSorted = nums.clone();
        Arrays.sort(numsSorted);

        /*
         * Build entry cache.
         *
         * O(n)
         */
        LOG.accept("Building entry cache.");

        Map<Integer, List<Integer>> numToPos = new HashMap<>();

        for (int i = 0; i < numsSorted.length; ++i) {
            List<Integer> list = numToPos.get(numsSorted[i]);
            if (list == null) {
                list = new LinkedList<>();
                numToPos.put(numsSorted[i], list);
            }
            list.add(i);
        }

        /*
         * Generate results.
         *
         * O(n^2)
         */
        LOG.accept("Generating results.");

        int i = 0;
        while (i < numsSorted.length - 2) {
            final int numA = numsSorted[i];
            int j = i + 1;
            while (j < numsSorted.length - 1) {
                final int numB = numsSorted[j];
                final int numC = 0 - (numA + numB);
                List<Integer> list = numToPos.get(numC);
                if (list != null) {
                    for (int k : list) {
                        if (k > j) {
                            List<Integer> resultEntry = new LinkedList<>();
                            resultEntry.add(numA);
                            resultEntry.add(numB);
                            resultEntry.add(numC);
                            result.add(resultEntry);
                            break;
                        }
                    }
                }
                while (j < numsSorted.length - 1 && numsSorted[j] == numB) {
                    ++j;
                }
            }
            while (i < numsSorted.length - 2 && numsSorted[i] == numA) {
                ++i;
            }
        }

        LOG.accept("Done.");

        return result;
    }
}