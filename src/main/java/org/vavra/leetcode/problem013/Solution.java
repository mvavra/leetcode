package org.vavra.leetcode.problem013;

class Solution {
    public int romanToInt(String s) {

        int result = 0;

        String[] symbolStringArray = new String[]     { "M",   "CM",  "D",  "CD",  "C",  "XC",  "L",  "XL",  "X",  "IX",  "V",  "IV",  "I" };
        int[] symbolValueArray = new int[]            { 1000,  900,   500,  400,   100,  90,    50,   40,    10,   9,     5,    4,     1 };
        boolean[] symbolRepeatedArray = new boolean[] { true,  false, true, false, true, false, true, false, true, false, true, false, true };

        int pos = 0;

        for (int i = 0; i < symbolStringArray.length; ++i) {
            final String symbolString = symbolStringArray[i];
            final int symbolValue = symbolValueArray[i];
            final boolean symbolRepeated = symbolRepeatedArray[i];

            while (substringMatch(s, pos, symbolString)) {
                result += symbolValue;
                pos += symbolString.length();
                if (!symbolRepeated) {
                    break;
                }
            }
        }

        return result;
    }

    private static boolean substringMatch(String s, int pos, String symbol) {
        for (int i = 0; i < symbol.length(); ++i) {
            if (pos+i >= s.length() || s.charAt(pos+i) != symbol.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}