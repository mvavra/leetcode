package org.vavra.leetcode.problem004;

class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        int[] nums = new int[nums1.length + nums2.length];

        /*
         * Weave
         */
        int i1 = 0;
        int i2 = 0;
        int i = 0;

        while (i1 < nums1.length && i2 < nums2.length) {
            if (nums1[i1] <= nums2[i2]) {
                nums[i++] = nums1[i1++];
            } else {
                nums[i++] = nums2[i2++];
            }
        }
        while (i1 < nums1.length) {
            nums[i++] = nums1[i1++];
        }
        while (i2 < nums2.length) {
            nums[i++] = nums2[i2++];
        }

        /*
         * Calculate median
         */

        if (nums.length % 2 == 1) {
            return nums[nums.length / 2];
        } else {
            return ((double)(nums[(nums.length-1)/2] + nums[nums.length/2])) / 2.0;
        }

    }
}
