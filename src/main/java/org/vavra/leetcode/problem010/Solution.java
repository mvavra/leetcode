package org.vavra.leetcode.problem010;

class Solution {
    public boolean isMatch(String s, String p) {

        if (p.isEmpty()) {
            return s.isEmpty();
        }

        final boolean wildcard = p.charAt(0) == '.';
        final boolean repeated = p.length() > 1 && p.charAt(1) == '*';

        if (repeated) {

            return isMatch(s, /*p.substring(2)*/skipRepeated(p)) || // CONSUME PATTERN ONLY

                    (!s.isEmpty() &&             // CONSUME STRING AND PATTERN
                            (wildcard || s.charAt(0) == p.charAt(0)) &&
                            isMatch(s.substring(1), /*p.substring(2)*/skipRepeated(p)))      ||

                    (!s.isEmpty() &&             // CONSUME STRING ONLY
                            (wildcard || s.charAt(0) == p.charAt(0)) &&
                            isMatch(s.substring(1), p));

        } else {

            return !s.isEmpty() &&
                    (wildcard || s.charAt(0) == p.charAt(0)) &&
                    isMatch(s.substring(1), p.substring(1));

        }

    }

    private static String skipRepeated(String p) {
        char c = p.charAt(0);
        int i = 2;
        while (i+1 < p.length() && p.charAt(i+1) == '*' && p.charAt(i) == c) {
            i += 2;
        }
        return p.substring(i);
    }
}
