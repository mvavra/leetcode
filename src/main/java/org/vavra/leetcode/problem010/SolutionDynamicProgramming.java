package org.vavra.leetcode.problem010;

class SolutionDynamicProgramming {
    public boolean isMatch(String s, String p) {

        Token[] tokens = new Token[p.length()];

        // parse pattern into tokens
        int tokenCount = 0;
        for (int i = 0; i < p.length();) {
            boolean repeated = (i+1 < p.length()) && (p.charAt(i+1) == '*');
            tokens[tokenCount] = new Token(p.charAt(i), repeated);
            i += repeated ? 2 : 1;
            ++tokenCount;
        }

        /*
         X ... token offset
         Y ... string offset
         */
        boolean matrix[][] = new boolean[tokenCount + 1][s.length() + 1];

        /*
         * POPULATE MATRIX BOUNDARIES
         */

        // empty pattern matches empty string
        matrix[tokenCount][s.length()] = true;

        // empty pattern never matches non-empty string
        for (int y = s.length()-1; y >= 0; --y) {
            matrix[tokenCount][y] = false;
        }

        // empty string only matches patters where all tokens are repeated
        for (int x = tokenCount-1; x >= 0; --x) {
            matrix[x][s.length()] = matrix[x+1][s.length()] && tokens[x].isRepeated();
        }

        /*
         * POPULATE MATRIX BODY
         */
        for (int x = tokenCount-1; x >= 0; --x) {
            for (int y = s.length()-1; y >= 0; --y) {
                if (tokens[x].isRepeated()) {
                    matrix[x][y] =
                        matrix[x+1][y] ||                                       // consume token only
                        (tokens[x].matches(s.charAt(y)) && matrix[x+1][y+1]) || // consume string and token
                        (tokens[x].matches(s.charAt(y)) && matrix[x][y+1]);     // consume string only
                } else {
                    matrix[x][y] = tokens[x].matches(s.charAt(y)) && matrix[x+1][y+1];
                }
            }
        }

        return matrix[0][0];
    }

    private static class Token {
        private char c;
        private boolean repeated;
        public Token(char c, boolean repeated) {
            this.c = c;
            this.repeated = repeated;
        }
        public boolean matches(char otherC) {
            return (this.c == '.') || (this.c == otherC);
        }
        public boolean isRepeated() {
            return repeated;
        }
    }
}
