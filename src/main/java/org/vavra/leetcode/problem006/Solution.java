package org.vavra.leetcode.problem006;

public class Solution {
    public String convert(String s, int numRows) {

        if (numRows == 0) {
            return "";
        } else if (numRows == 1) {
            return s;
        }

        StringBuilder sb = new StringBuilder(s.length());

        for (int row = 0; row < numRows; ++row) {
            // FIRST ROW or LAST ROW
            if (row == 0 || row == numRows-1) {
                int i = row;
                while (i < s.length()) {
                    sb.append(s.charAt(i));
                    i += 2 * (numRows - 1);
                }
            }
            // MIDDLE ROW (only executes for 3 rows and more)
            else {
                int i = row;
                while (i < s.length()) {
                    // append from downward direction
                    sb.append(s.charAt(i));
                    // move to bottom
                    i += numRows - row - 1;
                    // move upward to row
                    i += numRows - row - 1;
                    // append from upward direction
                    if (i < s.length()) {
                        sb.append(s.charAt(i));
                    }
                    // move to top
                    i += row;
                    i += row;
                }
            }
        }

        return sb.toString();
    }

}
