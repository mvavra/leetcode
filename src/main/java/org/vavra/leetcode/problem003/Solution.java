package org.vavra.leetcode.problem003;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public int lengthOfLongestSubstring(String s) {

        if (s.isEmpty()) {
            return 0;
        }

        Map<Character, Integer> lastOccurrenceIndex = new HashMap<>();

        final int n = s.length();
        int i = 0;
        int j = 0;
        int maxLength = 0;

        while (true) {

            // [i,j] is a unique substring. Is it longer than previous ones?
            maxLength = Math.max(maxLength, j-i+1);

            // Remember last occurrence of s[j].
            lastOccurrenceIndex.put(s.charAt(j), j);

            // Try extending to [i,j+1].
            ++j;

            // Did we run out of range?
            if (j >= n) {
                break;
            }

            // Is s[j] a duplicate character? If YES, jump i.
            final char newChar = s.charAt(j);
            if (lastOccurrenceIndex.containsKey(newChar) && lastOccurrenceIndex.get(newChar) >= i) {
                i = lastOccurrenceIndex.get(newChar) + 1;
            }
        }

        return maxLength;
    }
}
