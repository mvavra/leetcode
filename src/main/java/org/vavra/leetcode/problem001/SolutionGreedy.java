package org.vavra.leetcode.problem001;

import java.util.Arrays;

class SolutionGreedy {
    public int[] twoSum(int[] nums, int target) {

        /*
         * Build indices array.
         *
         * O(n)
         *
         */

        Integer[] indices = new Integer[nums.length];
        for (int i = 0; i < nums.length; ++i) {
            indices[i] = i;
        }

        /*
         * Sort indices array.
         *
         * O(n log n)
         *
         */
        Arrays.sort(indices, (index1, index2) -> Integer.compare(nums[index1], nums[index2]));

        /*
         * Find solution.
         *
         * O(n)
         *
         */

        int i = 0;
        int j = nums.length - 1;

        while (i < j) {
            final int indexA = indices[i];
            final int indexB = indices[j];
            final int numA = nums[indexA];
            final int numB = nums[indexB];
            final int sum = numA + numB;

            if (sum == target) {
                return new int[] {indexA, indexB};
            } else {
                if (sum < target) {
                    ++i;
                } else if (sum > target) {
                    --j;
                }
            }
        }

        throw new IllegalArgumentException("solution not found");
    }
}
