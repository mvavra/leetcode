package org.vavra.leetcode.problem001;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public int[] twoSum(int[] nums, int target) {
        final Map<Integer,Integer> cache = new HashMap<>();
        for (int i = 0; i < nums.length; ++i) {
            cache.put(nums[i], i);
        }
        for (int i = 0; i < nums.length-1; ++i) {
            final int a = nums[i];
            final int b = target - a;
            if (cache.containsKey(b)) {
                final Integer j = cache.get(b);
                if (i != j) {
                    return new int[] {i, j};
                }
            }
        }
        throw new IllegalArgumentException("solution not found");
    }
}
