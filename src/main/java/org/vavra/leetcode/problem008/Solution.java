package org.vavra.leetcode.problem008;

class Solution {
    public int myAtoi(String str) {

        final int n = str.length();

        int i = 0;

        while (i < n && str.charAt(i) == ' ') {
            ++i;
        }

        if (i >= n) {
            return 0;
        }

        boolean negative = false;

        if (str.charAt(i) == '-') {
            negative = true;
            ++i;
        } else if (str.charAt(i) == '+') {
            ++i;
        }

        if (i >= n) {
            return 0;
        }

        if (negative) {
            int number = 0;
            while (i < n) {
                char c = str.charAt(i);
                if (!(c >= '0' && c <= '9')) {
                    break;
                }
                if (number >= Integer.MIN_VALUE / 10) {
                    int number2 = (number * 10) - (c - '0');
                    if (number == 0 || number2 < number) {
                        number = number2;
                    } else {
                        return Integer.MIN_VALUE;
                    }
                } else {
                    return Integer.MIN_VALUE;
                }
                ++i;
            }
            return number;
        } else {
            int number = 0;
            while (i < n) {
                char c = str.charAt(i);
                if (!(c >= '0' && c <= '9')) {
                    break;
                }
                if (number <= Integer.MAX_VALUE / 10) {
                    int number2 = (number * 10) + (c - '0');
                    if (number == 0 || number2 > number) {
                        number = number2;
                    } else {
                        return Integer.MAX_VALUE;
                    }
                } else {
                    return Integer.MAX_VALUE;
                }
                ++i;
            }
            return number;
        }
    }
}
