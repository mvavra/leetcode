package org.vavra.leetcode.problem011;

class SolutionBruteForce {
    public int maxArea(int[] height) {
        int max = 0;
        for (int i = 0; i < height.length-1; ++i) {
            for (int j = i+1; j < height.length; ++j) {
                final int area = Math.min(height[i], height[j]) * (j-i);
                if (area > max) {
                    max = area;
                }
            }
        }
        return max;
    }
}
