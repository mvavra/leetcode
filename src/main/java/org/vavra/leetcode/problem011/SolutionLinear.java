package org.vavra.leetcode.problem011;

class SolutionLinear {
    public int maxArea(int[] height) {
        int max = 0;

        // two pointers - start at very left and very right
        int left = 0;
        int right = height.length - 1;

        // step by step narrowing
        // move the shorter side
        while (left < right) {
            if (height[left] <= height[right]) {
                max = Math.max(max, (right - left) * height[left]);
                ++left;
            } else {
                max = Math.max(max, (right - left) * height[right]);
                --right;
            }
        }

        return max;
    }
}
