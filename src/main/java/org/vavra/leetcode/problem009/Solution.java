package org.vavra.leetcode.problem009;

class Solution {
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        long orig = x;
        long mirror = 0;
        while (x != 0) {
            mirror *= 10;
            mirror += x % 10;
            x /= 10;
        }
        return orig == mirror;
    }
}
