package org.vavra.leetcode.problem020;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Solution {
    public boolean isValid(String s) {
        Stack<BracketType> stack = new Stack();
        for (int i = 0; i < s.length(); ++i) {
            final char c = s.charAt(i);
            if (c == '(' || c == '{' || c == '[') {
                stack.push(BracketType.byOpeningChar(c));
            } else if (c == ')' || c == '}' || c == ']') {
                if (stack.isEmpty()) {
                    return false;
                }
                BracketType matching = stack.pop();
                if (matching.getClosingChar() != c) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    private enum BracketType {
        ROUND('(', ')'),
        CURLY('{', '}'),
        SQUARE('[', ']');

        private static final Map<Character, BracketType> MAP = new HashMap<>();
        static {
            for (BracketType bt : BracketType.values()) {
                MAP.put(bt.openingChar, bt);
            }
        }

        static BracketType byOpeningChar(char c) {
            return MAP.get(c);
        }

        private final char openingChar;
        private final char closingChar;

        BracketType(char openningChar, char closingChar) {
            this.openingChar = openningChar;
            this.closingChar = closingChar;
        }

        char getClosingChar() {
            return closingChar;
        }
    }

}
