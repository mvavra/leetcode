package org.vavra.leetcode.problem014;

public class Solution {
    public String longestCommonPrefix(String[] strs) {
        final int n = strs.length;
        if (n == 0) {
            return "";
        }

        if (strs[0].length() == 0) {
            return "";
        }

        if (n == 1) {
            return strs[0];
        }

        int minLength = Integer.MAX_VALUE;
        for (int i = 0; i < n; ++i) {
            minLength = Math.min(minLength, strs[i].length());
        }

        int lastAddedIndex = -1;

        for (int i = 0; i < minLength; ++i) {
            final char nextChar = strs[0].charAt(i);
            boolean add = true;
            for (int j = 1; j < n; ++j) {
                if (strs[j].charAt(i) != nextChar) {
                    add = false;
                    break;
                }
            }
            if (add) {
                lastAddedIndex = i;
            } else {
                break;
            }
        }

        return strs[0].substring(0, lastAddedIndex+1);
    }
}
