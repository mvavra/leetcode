package org.vavra.leetcode.common;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class StringSupplierLogger implements Consumer<Supplier<String>> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    @Override
    public void accept(Supplier<String> stringSupplier) {
        StringBuilder sb = new StringBuilder();
        sb.append(FORMATTER.format(LocalTime.now()));
        sb.append(": ");
        sb.append(stringSupplier.get());
        System.out.println(sb.toString());
    }
}
