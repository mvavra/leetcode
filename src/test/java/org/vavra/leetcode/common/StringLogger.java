package org.vavra.leetcode.common;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

public class StringLogger implements Consumer<String> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    @Override
    public void accept(String msg) {
        StringBuilder sb = new StringBuilder();
        sb.append(FORMATTER.format(LocalTime.now()));
        sb.append(": ");
        sb.append(msg);
        System.out.println(sb.toString());
    }
}
