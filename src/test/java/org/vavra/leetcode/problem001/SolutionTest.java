package org.vavra.leetcode.problem001;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertArrayEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;
    private SolutionGreedy solutionGreedy;

    @Before
    public void setUp() {
        solution = new Solution();
        solutionGreedy = new SolutionGreedy();
    }

    private Object[] parameters() {
        return new Object[] {
                new Object[] { new int[] {2, 7, 11, 15}, 9, new int[] {0, 1} }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(int[] nums, int target, int[] result) {
        assertArrayEquals(result, solution.twoSum(nums, target));
    }

    @Test
    @Parameters(method = "parameters")
    public void testGreedy(int[] nums, int target, int[] result) {
        assertArrayEquals(result, solutionGreedy.twoSum(nums, target));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoSolution() {
        solution.twoSum(new int[] {1, 2, 3, 4, 5}, 100);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoSolutionGreedy() {
        solutionGreedy.twoSum(new int[] {1, 2, 3, 4, 5}, 100);
    }

}
