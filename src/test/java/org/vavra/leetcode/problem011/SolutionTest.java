package org.vavra.leetcode.problem011;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private SolutionBruteForce solutionBruteForce;
    private SolutionLinear solutionLinear;

    @Before
    public void setUp() {
        solutionBruteForce = new SolutionBruteForce();
        solutionLinear = new SolutionLinear();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { new int[] {1, 8, 6, 2, 5, 4, 8, 3, 7}, 49 }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void testBruteForce(int[] height, int result) {
        assertEquals(result, solutionBruteForce.maxArea(height));
    }

    @Test
    @Parameters(method = "parameters")
    public void testLinear(int[] height, int result) {
        assertEquals(result, solutionLinear.maxArea(height));
    }

}
