package org.vavra.leetcode.problem008;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { "42", 42 },
                new Object[] { "   -42", -42 },
                new Object[] { "4193 with words", 4193 },
                new Object[] { "words and 987", 0 },
                new Object[] { "-91283472332", -2147483648 },
                new Object[] { "9223372036854775808", 2147483647 },
                new Object[] { "  0000000000012345678", 12345678 }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String str, int result) {
        assertEquals(result, solution.myAtoi(str));
    }

}
