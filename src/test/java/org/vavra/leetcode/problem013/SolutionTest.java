package org.vavra.leetcode.problem013;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    private Object[] parameters() {
        return new Object[] {
                new Object[] { "III", 3 },
                new Object[] { "IV", 4 },
                new Object[] { "IX", 9 },
                new Object[] { "LVIII", 58 },
                new Object[] { "MCMXCIV", 1994 }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String s, int result) {
        assertEquals(result, solution.romanToInt(s));
    }
}
