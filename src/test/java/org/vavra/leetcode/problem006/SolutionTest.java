package org.vavra.leetcode.problem006;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { "PAYPALISHIRING", 0, "" },
                new Object[] { "PAYPALISHIRING", 1, "PAYPALISHIRING" },
                new Object[] { "PAYPALISHIRING", 2, "PYAIHRNAPLSIIG" },
                new Object[] { "PAYPALISHIRING", 3, "PAHNAPLSIIGYIR" },
                new Object[] { "PAYPALISHIRING", 4, "PINALSIGYAHRPI" }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String s, int numRows, String result) {
        assertEquals(result, solution.convert(s, numRows));
    }

}
