package org.vavra.leetcode.problem014;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { new String[] { "flower", "flow", "flight" }, "fl" },
                new Object[] { new String[] { "dog", "racecar", "car" }, "" },
                new Object[] { new String[] { }, "" },
                new Object[] { new String[] { "", "flow", "flight" }, "" }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String[] strs, String result) {
        assertEquals(result, solution.longestCommonPrefix(strs));
    }

}
