package org.vavra.leetcode.problem007;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { 123, 321 },
                new Object[] { -123, -321 },
                new Object[] { 120, 21 },
                new Object[] { 1534236469, 0 }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(int x, int result) {
        assertEquals(result, solution.reverse(x));
    }

}
