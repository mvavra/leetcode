package org.vavra.leetcode.problem003;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { "abcabcbb", 3 },
                new Object[] { "bbbbb", 1 },
                new Object[] { "pwwkew", 3 },
                new Object[] { "", 0 },
                new Object[] { "a", 1 },
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String s, int result) {
        assertEquals(result, solution.lengthOfLongestSubstring(s));
    }

}
