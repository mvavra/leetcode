package org.vavra.leetcode.problem020;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    private Object[] parameters() {
        return new Object[] {
                new Object[] { "()", true },
                new Object[] { "()[]{}", true },
                new Object[] { "(]", false },
                new Object[] { "([)]", false },
                new Object[] { "{[]}", true },
                new Object[] { "", true },
                new Object[] { "(", false }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String s, boolean valid) {
        assertEquals(valid, solution.isValid(s));
    }

}