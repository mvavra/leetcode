package org.vavra.leetcode.problem012;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private SolutionSimple solutionSimple;
    private SolutionTreemap solutionTreemap;
    private SolutionExplicit solutionExplicit;
    private SolutionIterateDenoms solutionIterateDenoms;
    private SolutionFastest solutionFastest;

    @Before
    public void setUp() {
        solutionSimple = new SolutionSimple();
        solutionTreemap = new SolutionTreemap();
        solutionExplicit = new SolutionExplicit();
        solutionIterateDenoms = new SolutionIterateDenoms();
        solutionFastest = new SolutionFastest();
    }

    /*
      Input is guaranteed to be within the range from 1 to 3999.
     */
    public Object[] parameters() {
        return new Object[] {
                new Object[] { 3, "III" },
                new Object[] { 4, "IV" },
                new Object[] { 9, "IX" },
                new Object[] { 58, "LVIII"},
                new Object[] { 58, "LVIII"},
                new Object[] { 1994, "MCMXCIV"},
                new Object[] { 3200, "MMMCC"},
                new Object[] { 3888, "MMMDCCCLXXXVIII"}
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(int num, String result) {
        assertEquals(result, solutionSimple.intToRoman(num));
    }

    @Test
    @Parameters(method = "parameters")
    public void testTreemap(int num, String result) {
        assertEquals(result, solutionTreemap.intToRoman(num));
    }

    @Test
    @Parameters(method = "parameters")
    public void testExplicit(int num, String result) {
        assertEquals(result, solutionExplicit.intToRoman(num));
    }

    @Test
    @Parameters(method = "parameters")
    public void testIterateDenoms(int num, String result) {
        assertEquals(result, solutionIterateDenoms.intToRoman(num));
    }

    @Test
    @Parameters(method = "parameters")
    public void testFastest(int num, String result) {
        assertEquals(result, solutionFastest.intToRoman(num));
    }

}
