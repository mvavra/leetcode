package org.vavra.leetcode.problem002;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Stack;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { 342, 465, 807 },
                new Object[] { 1,   999, 1000 }
        };
    }

    /*
    Convert 342 to 2->4->3.
     */
    private static ListNode intToList(int number) {
        ListNode first = null;
        ListNode last = null;
        while (number != 0) {
            ListNode node = new ListNode(number % 10);
            if (last != null) {
                last.next = node;
            }
            last = node;
            if (first == null) {
                first = node;
            }
            number /= 10;
        }
        return first;
    }

    /*
    Convert 2->4->3 to 342.
     */
    private static int listToInt(ListNode list) {
        Stack<Integer> stack = new Stack<>();
        while (list != null) {
            stack.push(list.val);
            list = list.next;
        }
        int result = 0;
        while (!stack.isEmpty()) {
            result *= 10;
            result += stack.pop();
        }
        return result;
    }

    @Test
    @Parameters(method = "parameters")
    public void test(int num1, int num2, int result) {
        assertEquals(result, listToInt(solution.addTwoNumbers(intToList(num1), intToList(num2))));
    }

}
