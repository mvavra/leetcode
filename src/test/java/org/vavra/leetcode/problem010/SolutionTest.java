package org.vavra.leetcode.problem010;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;
    private SolutionDynamicProgramming solutionDynamicProgramming;

    @Before
    public void setUp() {
        solution = new Solution();
        solutionDynamicProgramming = new SolutionDynamicProgramming();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { "aa", "a", false },
                new Object[] { "aa", "a*", true },
                new Object[] { "ab", ".*", true },
                new Object[] { "aab", "c*a*b", true },
                new Object[] { "mississippi", "mis*is*p*.", false },
                new Object[] { "aaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*c", false },
                new Object[] { "", "", true },
                new Object[] { "", "a*", true },
                new Object[] { "", "a", false }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String s, String p, boolean result) {
        assertEquals(result, solution.isMatch(s, p));
    }

    @Test
    @Parameters(method = "parameters")
    public void testDynamicProgramming(String s, String p, boolean result) {
        assertEquals(result, solutionDynamicProgramming.isMatch(s, p));
    }

}
