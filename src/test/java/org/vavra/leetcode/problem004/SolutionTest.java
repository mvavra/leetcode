package org.vavra.leetcode.problem004;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { new int[] {1, 3}, new int[] {2}, 2.0 },
                new Object[] { new int[] {1, 2}, new int[] {3, 4}, 2.5 },
                new Object[] { new int[] {1, 2}, new int[] {}, 1.5 },
                new Object[] { new int[] {1, 2, 3}, new int[] {}, 2 }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(int[] nums1, int[] nums2, double result) {
        assertEquals(result, solution.findMedianSortedArrays(nums1, nums2), 0.0);
    }

}
