package org.vavra.leetcode.problem005;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { "babad", "bab" },
                new Object[] { "cbbd", "bb" },
                new Object[] { "a", "a" },
                new Object[] { "", "" },
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(String s, String result) {
        assertEquals(result, solution.longestPalindrome(s));
    }

}
