package org.vavra.leetcode.problem009;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SolutionTest {

    private Solution solution;

    @Before
    public void setUp() {
        solution = new Solution();
    }

    public Object[] parameters() {
        return new Object[] {
                new Object[] { 121, true },
                new Object[] { -121, false },
                new Object[] { 10, false }
        };
    }

    @Test
    @Parameters(method = "parameters")
    public void test(int x, boolean result) {
        assertEquals(result, solution.isPalindrome(x));
    }

}
